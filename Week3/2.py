import matplotlib.pyplot as plt
import numpy as np

Sx = np.arange(0, 3*np.pi,np.pi/10 )
Sy = np.sin(Sx)

Cx = np.arange(0, 3*np.pi, np.pi/10)
Cy = np.cos(Cx)

plt.axis([0 ,3*np.pi, -1, 1])
plt.plot(Sx, Sy, color='blue')
plt.plot(Cx, Cy, color='red')
plt.title('sin&cos')
plt.legend(['y=sinx', 'y=cosx'])
plt.xlabel('x')
plt.ylabel('y', rotation=0)

plt.show()
