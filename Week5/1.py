import matplotlib.pyplot as plt
import numpy as np

pi = np.pi
n1 = np.arange(0, 2*pi, 2*pi/40)
n2 = np.arange(0, 8*pi, 2*pi/40)
n3 = np.arange(-4*pi, 4*pi, 2*pi/40)


def MH(n):
    return (1+0.8**2-2*0.8*np.cos(n))**(-0.5)


def PH(n):
    return -1*np.arctan(0.8*np.sin(n)/(1-(0.8*np.cos(n))))


def single(w, n):
    return np.cos(w*pi*n)


def output(w, n):
    ans = np.zeros(len(n))
    input1 = np.cos(w*pi*n)
    ans[0] = input1[0]
    for i in range(1, len(n)):
        ans[i] = 0.8*ans[i-1]+input1[i]
    return ans


x1 = np.arange(0, 160)
x2 = np.arange(0, 32)

plt.figure(1)
plt.subplot(3, 1, 1)
plt.stem(n1, PH(n1))
plt.subplot(3, 1, 2)
plt.stem(n2, PH(n2))
plt.subplot(3, 1, 3)
plt.stem(n3, PH(n3))

plt.figure(2)
plt.subplot(3, 1, 1)
plt.stem(n1, MH(n1))
plt.subplot(3, 1, 2)
plt.stem(n2, MH(n2))
plt.subplot(3, 1, 3)
plt.stem(n3, MH(n3))

plt.figure(3)
plt.subplot(2, 1, 1)
plt.stem(x1, single(0.05, x1))
plt.subplot(2, 1, 2)
plt.stem(x2, single(0.25, x2))

plt.figure(4)
plt.subplot(2, 1, 1)
plt.stem(x1, output(0.05, x1))
plt.subplot(2, 1, 2)
plt.stem(x2, output(0.25, x2))


plt.show()
