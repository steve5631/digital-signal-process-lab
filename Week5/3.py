import matplotlib.pyplot as plt
import numpy as np


def w(x, k):
    y = k*np.random.randn(1, 21)
    return y[0]+x


def cc(x, y):
    ans = np.zeros(41)
    for i in range(-20, 21):
        if i < 0:
            tmp = np.roll(y, i)
            tmp[i:] = 0
        elif i > 0:
            tmp = np.roll(y, i)
            tmp[0:i] = 0
        elif i == 0:
            tmp = y
        x = np.array(x)
        tmp = np.array(tmp)
        ans[i+20] = np.sum(x*tmp)
    return ans


n = np.arange(-20, 21)
m = np.arange(0, 21)
s = np.zeros(21)
x = np.zeros(21)
t = np.zeros(21)
y = np.zeros(21)


s[0:5] = 1
x = np.roll(s, 10)  
x[0:10] = 0
for i in range(0, len(s)):
    t[i] = 0.6**(i)*s[i]
y = np.roll(t, 10)
y[0:10] = 0


print()
plt.figure(1)
plt.subplot(2, 1, 1)
plt.stem(n, cc(x, y))
plt.subplot(2, 1, 2)
plt.stem(n, cc(x, s))

plt.figure(2)
plt.subplot(4, 1, 1)
plt.stem(n, cc(w(x, 0), x))
plt.subplot(4, 1, 2)
plt.stem(n, cc(w(x, 5), x))
plt.subplot(4, 1, 3)
plt.stem(n, cc(w(x, 1), x))
plt.subplot(4, 1, 4)
plt.stem(n, cc(w(x, 2), x))

plt.figure(3)
plt.subplot(4, 1, 1)
plt.stem(n, cc(w(y, 0), y))
plt.subplot(4, 1, 2)
plt.stem(n, cc(w(y, 5), y))
plt.subplot(4, 1, 3)
plt.stem(n, cc(w(y, 1), y))
plt.subplot(4, 1, 4)
plt.stem(n, cc(w(y, 2), y))


plt.show()
