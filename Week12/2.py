import numpy as np
import matplotlib.pyplot as plt
from scipy import signal

u = np.zeros(1024)
u[0:1024] = 1

u20 = np.roll(u, 20)
u20[0:20] = 0

a = 0.5
N1 = 1

x1 = np.zeros(1024)
x2 = np.zeros(1024)
x3 = np.zeros(1024)
for n in range(0, 1024):
    x1[n] = (n+1)*(a**n)
    x2[n] = a**np.abs(n)
x1 *= u


w = np.arange(0, 2*np.pi, 2*np.pi/1024)
fftX1 = np.zeros(1024)
fftX2 = np.zeros(1024)

for k in range(0, 1024):
    fftX1[k] = (1-a*np.exp(-1j*w[k]))**(-2)
    fftX2[k] = (1-a**2)/(1-(2*a*np.cos(w[k]))+a**2)


plt.figure(1)
plt.subplot(2, 1, 1)
plt.stem(np.abs(fftX1))
plt.subplot(2, 1, 2)
plt.stem(np.fft.fft(x1))

plt.figure(2)
plt.subplot(2, 1, 1)
plt.stem(np.abs(fftX2))
plt.subplot(2, 1, 2)
plt.stem(np.fft.fft(x2))

plt.show()
