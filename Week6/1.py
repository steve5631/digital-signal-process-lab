import numpy as np
import matplotlib.pyplot as plt
pi = np.pi
range10 = np.arange(-4*pi, 4*pi, 8*pi/1000)
range2 = np.arange(-4*pi, 4*pi, 8*pi/200)


def DFT(single, N):
    output = np.zeros(len(N), dtype=complex)
    tmp = np.zeros(len(single), dtype=complex)
    for i in range(0, len(N)):
        for j in range(0, len(single)):
            tmp[j] = single[j]*np.exp(-1j*j*N[i])
        output[i] = np.sum(tmp)
    return output


x2 = np.zeros(101, dtype=complex)
for i in range(0, 101):
    x2[i] = (0.9*np.exp(1j*pi/3))**i

x3 = np.zeros(201, dtype=complex)
for i in range(0, 201):
    x3[i] = (-0.9)**(i-100)

x4 = np.zeros(101, dtype=complex)
x4[0:6] = 1


x5 = np.zeros(101, dtype=complex)
x5[0:7] = 1


plt.figure(1)
plt.subplot(2, 1, 1)
plt.stem(range10, np.absolute(DFT(x2, range10)))
plt.subplot(2, 1, 2)
plt.stem(range2, np.absolute(DFT(x2, range2)))

plt.figure(2)
plt.subplot(2, 1, 1)
plt.stem(range10, np.absolute(DFT(x3, range10)))
plt.subplot(2, 1, 2)
plt.stem(range2, np.absolute(DFT(x3, range2)))

plt.figure(3)
plt.subplot(2, 1, 1)
plt.plot(range10, np.absolute(DFT(x4, range10)))
plt.subplot(2, 1, 2)
plt.plot(range2, np.absolute(DFT(x4, range2)))

plt.figure(4)
plt.subplot(2, 1, 1)
plt.plot(range10, np.absolute(DFT(x5, range10)))
plt.subplot(2, 1, 2)
plt.plot(range2, np.absolute(DFT(x5, range2)))

plt.show()
