import matplotlib.pyplot as plt
import numpy as np

n = np.arange(0, 200)
p = np.pi
a = 0.1*np.random.randn(200,)

x0 = np.cos(0.1*p*n)+2*np.sin(0.3*p*n) + a
x2 = 3*np.cos(0.1*p*n+p/3)+1.5*np.sin(0.5*p*n) + a
x4 = np.cos(0.11*p*n) + a

plt.figure(1)
plt.subplot(5, 1, 1)
plt.plot(n, x0)
plt.title("x0")
plt.xlabel("n")
plt.ylabel("x0")
plt.grid("ON")

plt.figure(1)
plt.subplot(5, 1, 3)
plt.plot(n, x2)
plt.title("x2")
plt.xlabel("n")
plt.ylabel("x2")
plt.grid("ON")

plt.figure(1)
plt.subplot(5, 1, 5)
plt.plot(n, x4)
plt.title("x4")
plt.xlabel("n")
plt.ylabel("x4")
plt.grid("ON")

plt.show()
