import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
# 10 -> 0
# 20 -> 10
xlable2 = np.array(range(0, 13))
xlable1 = np.array(range(-10, 11))
x = np.zeros(21)
x[10] = 1
y1 = np.zeros(21)
y2 = np.zeros(21)

X = (-0.7) ** np.arange(0, 13)
Y = np.zeros(13)

for i in range(10, len(x)):
    y1[i] = -0.8 * y1[i-1] + 0.5*x[i]-0.3*x[i-1]
    y2[i] = x[i]+0.4*x[i-1] + 0.16*x[i-2]

for i in range(9, -1):
    print(i)
    if(i > 0):
        y1[i] = -0.8 * y1[i-1] + 0.5*x[i]-0.3*x[i-1]
    if (i > 1):
        y2[i] = x[i]+0.4*x[i-1] + 0.16*x[i-2]

for i in range(0, len(X)):
    if i == 0:
        Y[i] = X[i]
    if i == 1:
        Y[i] = -0.9*Y[i-1]+X[i]-0.5*X[i-1]
    if i == 2:
        Y[i] = -0.9*Y[i-1]+X[i]-0.5*X[i-1]+0.3*X[i-2]
    if i >= 3:
        Y[i] = -0.9*Y[i-1]+0.4*Y[i-3]+X[i]-0.5*X[i-1]+0.3*X[i-2]

a = np.array([1, 0.9, 0, -0.4])
b = np.array([1, -0.5, 0.3, 0])
Y_S = signal.lfilter(b, a, X)


plt.figure(1)
plt.title("(1)")
plt.xlabel("x[n]")
plt.ylabel("y[n]")
plt.stem(xlable1, y1)

plt.figure(2)
plt.title("(2)")
plt.xlabel("x[n]")
plt.ylabel("y[n]")
plt.stem(xlable1, y2)

plt.figure(3)
plt.title("(4)")
plt.xlabel("x[n]")
plt.ylabel("y[n]")
plt.stem(xlable2, Y)

plt.figure(4)
plt.title("(5)")
plt.xlabel("x[n]")
plt.ylabel("y[n]")
plt.stem(xlable2, Y_S)

plt.show()
