import numpy as np
from matplotlib import pyplot as plt
import single

n0 = int(input("n0:"))
n1 = int(input("n1:"))
n2 = int(input("n2:"))

if(n1 <= n0 and n0 <= n2):
    impseqData = single.impseq(n0, n1, n2)
    stepseqData = single.stepseq(n0, n1, n2)
    n = np.arange(n1, n2+1)

    plt.subplot(2, 1, 1)
    plt.stem(n, impseqData)
    plt.subplot(2, 1, 2)
    plt.stem(n, stepseqData)

    plt.show()
else:
    print("value error must n1 <= n0 <= n2")
