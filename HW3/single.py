import numpy as np


def impseq(n0, n1, n2):
    x = np.zeros(n2-n1+1)
    x[n0-n1] = 1
    return x


def stepseq(n0, n1, n2):
    x = np.zeros(n2-n1+1)
    x[(n0-n1):] = 1
    return x
