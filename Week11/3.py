import soundfile as sf
from scipy.fftpack import fft, ifft
import numpy as np
import matplotlib.pyplot as plt
import time

filename = 'thwap3.wav'
y, fs = sf.read(filename)

dft_time = np.zeros(2048, np.float)
fft_time = np.zeros(2048, np.float)


def DFT_function(x, N):
    j = 0+1j
    X = np.arange(0, N)
    k = np.arange(0, len(x))
    a = np.reshape(X, (len(X), 1))
    b = np.reshape(k, (1, len(X)))
    X = np.exp((-j*2*np.pi*np.dot(a, b)/len(x)))
    Xk = X@(x.T)
    return Xk


for i in range(0, 2048):
    start_time_1 = time.time()
    DFT_0 = DFT_function(y[0:i], i)
    end_time_1 = time.time()

    dft_time[i] = end_time_1-start_time_1

for i in range(0, 2048):
    start_time_2 = time.time()
    DFT_1 = fft(y[0:i], i+1)
    end_time_2 = time.time()

    fft_time[i] = end_time_2-start_time_2

plt.figure(1)
plt.plot(dft_time)
plt.plot(fft_time, 'y')
