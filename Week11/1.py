import numpy as np
j = 0+1j


def DFT_func_F(xn, N):
    M = np.zeros((N, len(xn)), dtype=complex)
    for k in range(0, N):
        for i in range(0, len(xn)):
            M[k][i] = np.exp(-j*2*np.pi*i*k/len(xn))
    return M@xn.T


def IDFT_func_F(XK, N):
    M = np.zeros((N, len(XK)), dtype=complex)
    for k in range(0, N):
        for i in range(0, len(XK)):
            M[k][i] = np.exp(j*2*np.pi*i*k/len(XK))/len(XK)
    return M@XK.T


# block_conv,
x = np.arange(1, 21)
h = np.array([1, 0, -1])
n = 6
m = h.size
#y = block_conv(x,h,n,m)

x1 = np.hstack([np.zeros(2), x[0:4]])
x2 = x[2:8]
x3 = x[6:12]
x4 = x[10:16]
x5 = x[14:20]
x6 = np.hstack([x[18:20], np.zeros(4)])

DFT_h = DFT_func_F(np.hstack([h, np.zeros(3)]), 6)
DFT_x1 = DFT_func_F(x1, 6)
DFT_x2 = DFT_func_F(x2, 6)
DFT_x3 = DFT_func_F(x3, 6)
DFT_x4 = DFT_func_F(x4, 6)
DFT_x5 = DFT_func_F(x5, 6)
DFT_x6 = DFT_func_F(x6, 6)

IDFT_1 = IDFT_func_F(DFT_h*DFT_x1, 6)
IDFT_2 = IDFT_func_F(DFT_h*DFT_x2, 6)
IDFT_3 = IDFT_func_F(DFT_h*DFT_x3, 6)
IDFT_4 = IDFT_func_F(DFT_h*DFT_x4, 6)
IDFT_5 = IDFT_func_F(DFT_h*DFT_x5, 6)
IDFT_6 = IDFT_func_F(DFT_h*DFT_x6, 6)

y = np.hstack([IDFT_1[2:6], IDFT_2[2:6], IDFT_3[2:6],
               IDFT_4[2:6], IDFT_5[2:6], IDFT_6[2:4]])

print(np.real(y))
print(len(np.real(y)))
