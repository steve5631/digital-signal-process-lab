import numpy as np
j = 0+1j


def DFT_func_F(xn, N):
    M = np.zeros((N, len(xn)), dtype=complex)
    for k in range(0, N):
        for i in range(0, len(xn)):
            M[k][i] = np.exp(-j*2*np.pi*i*k/len(xn))
    return M@xn.T


def IDFT_func_F(XK, N):
    M = np.zeros((N, len(XK)), dtype=complex)
    for k in range(0, N):
        for i in range(0, len(XK)):
            M[k][i] = np.exp(j*2*np.pi*i*k/len(XK))/len(XK)
    return M@XK.T

# def block_conv(x,h,n,m):
