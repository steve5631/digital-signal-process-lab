import numpy as np
import matplotlib.pyplot as plt
pi = np.pi


def my_DFT(xn, N):
    output = np.zeros(N, dtype=complex)
    tmp = np.zeros(len(xn), dtype=complex)
    for i in range(0, N):
        for j in range(0, len(xn)):
            tmp[j] = xn[j]*np.exp(-1j*j*i*2*pi/N)
        output[i] = np.sum(tmp)
    return output


def my_IDFT(Xk, N):
    output = np.zeros(N, dtype=complex)
    tmp = np.zeros(len(Xk), dtype=complex)
    for i in range(0, N):
        for j in range(0, len(Xk)):
            tmp[j] = Xk[j]*np.exp(1j*j*i*2*pi/N)
        output[i] = np.sum(tmp)/N
    return output


def M_my_DFT(xn, N):
    x = np.zeros((N, N), dtype=complex)
    for i in range(0, N):
        for j in range(0, N):
            x[i][j] = np.exp(-1j*j*2*pi*i/N)
    return xn.dot(x)


def M_my_IDFT(Xk, N):
    x = np.zeros((N, N), dtype=complex)
    for i in range(0, N):
        for j in range(0, N):
            x[i][j] = np.exp(1j*j*2*pi*i/N)
    return x.dot(Xk)/N


def M_my_IDFTX(Xk, N):
    x = np.zeros((64, 64), dtype=complex)
    for i in range(0, N):
        for j in range(0, N):
            x[i][j] = np.exp(1j*j*2*pi*i/N)
    return 1/N*x.dot(Xk)


x2 = np.zeros(101, dtype=complex)
for i in range(0, 101):
    x2[i] = (0.9*np.exp(1j*pi/3))**i

x3 = np.zeros(201, dtype=complex)
for i in range(0, 201):
    x3[i] = (-0.9)**(i-100)


def singleOne(N):
    x = np.zeros(N, dtype=complex)
    x[0:5] = 1
    return x


def singleTwo(N):
    x = np.zeros(N, dtype=complex)
    for i in range(0, 45):
        x[i] = 0.95**i
    return x


plt.figure(1)
plt.subplot(4, 1, 1)
plt.stem(np.arange(0, 5), np.absolute(my_DFT(singleOne(5), 5)))
plt.subplot(4, 1, 2)
plt.stem(np.arange(0, 8), np.absolute(my_DFT(singleOne(8), 8)))
plt.subplot(4, 1, 3)
plt.stem(np.arange(0, 16), np.absolute(my_DFT(singleOne(16), 16)))
plt.subplot(4, 1, 4)
plt.stem(np.arange(0, 32), np.absolute(my_DFT(singleOne(32), 32)))

plt.figure(2)
plt.subplot(4, 1, 1)
plt.stem(np.arange(0, 64), np.absolute(
    my_DFT(np.absolute(my_IDFT(singleOne(5), 64)), 64)))
plt.subplot(4, 1, 2)
plt.stem(np.arange(0, 64), np.absolute(
    my_DFT(np.absolute(my_IDFT(singleOne(8), 64)), 64)))
plt.subplot(4, 1, 3)
plt.stem(np.arange(0, 64), np.absolute(
    my_DFT(np.absolute(my_IDFT(singleOne(16), 64)), 64)))
plt.subplot(4, 1, 4)
plt.stem(np.arange(0, 64), np.absolute(
    my_DFT(np.absolute(my_IDFT(singleOne(32), 64)), 64)))


# plt.figure(3)
# plt.subplot(2, 1, 1)
# plt.stem(np.arange(0, 45), np.absolute(M_my_IDFT(singleTwo(45), 45)))
# plt.subplot(2, 1, 2)
# plt.stem(np.arange(0, 100), np.absolute(M_my_IDFT(singleTwo(100), 100)))

# plt.figure(4)
# plt.subplot(2, 1, 1)
# plt.stem(np.arange(0, 45), np.absolute(M_my_IDFT(singleTwo(45), 45)))
# plt.subplot(2, 1, 2)
# plt.stem(np.arange(0, 45), np.absolute(
#     M_my_DFT(M_my_IDFT(singleTwo(45), 45), 45)))

# plt.figure(5)
# plt.subplot(2, 1, 1)
# plt.stem(np.arange(0, 100), np.absolute(M_my_IDFT(singleTwo(100), 100)))
# plt.subplot(2, 1, 2)
# plt.stem(np.arange(0, 100), np.absolute(
#     M_my_DFT(M_my_IDFT(singleTwo(100), 100), 100)))

# p = M_my_IDFT(singleTwo(100), 100)
# q = p[::2]
# r = p[::4]
# plt.figure(6)
# plt.subplot(2, 1, 1)
# plt.stem(np.arange(0, 50), np.absolute(
#     M_my_DFT(q, 50)))
# plt.subplot(2, 1, 2)
# plt.stem(np.arange(0, 25), np.absolute(
#     M_my_DFT(r, 25)))


plt.show()
