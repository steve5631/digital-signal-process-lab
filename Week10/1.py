import numpy as np
import matplotlib.pyplot as plt
pi = np.pi


def my_DFT(xn, N):
    output = np.zeros(N, dtype=complex)
    tmp = np.zeros(len(xn), dtype=complex)
    for i in range(0, N):
        for j in range(0, len(xn)):
            tmp[j] = xn[j]*np.exp(-1j*j*i*2*pi/len(xn))
        output[i] = np.sum(tmp)
    return output


def my_IDFT(Xk, N):
    output = np.zeros(N, dtype=complex)
    tmp = np.zeros(len(Xk), dtype=complex)
    for i in range(0, N):
        for j in range(0, len(Xk)):
            tmp[j] = Xk[j]*np.exp(1j*j*i*2*pi/len(Xk))
        output[i] = np.sum(tmp)
    return output/len(Xk)


def oneA(tot):
    u = np.zeros(tot)
    u[0:tot] = 1
    u256 = np.roll(u, 256)
    u256[0:256] = 0
    x = np.zeros(tot)
    for i in range(0, tot):
        x[i] = ((2/3)**i)+((-15/16)**i)
    return x*(u-u256)


def oneB(tot):
    u = np.zeros(tot)
    u[0:tot] = 1
    u256 = np.roll(u, 256)
    u256[0:256] = 0
    x = np.zeros(tot)
    for i in range(0, tot):
        x[i] = np.exp(-0.1*i)*(np.sin(pi*i/5)+np.cos(0.3*pi*i))
    return x*(u-u256)


def TwoOne(tot):
    u = np.zeros(tot)
    u[0:tot] = 1
    u10 = np.roll(u, 10)
    u10[0:10] = 0
    return u-u10


def TwoX2a(tot):
    u = np.zeros(tot)
    x = np.zeros(tot)
    u[0:tot] = 1
    for i in range(0, 100):
        x[i] = i*u[i]
    return x


def TwoX2b(tot):
    u = np.zeros(tot)
    x = np.zeros(tot)
    u[0:tot] = 1
    for i in range(0, 100):
        x[i] = (u[i])*((1/3)**i)
    return x


def IFFT(x, n):
    N = len(x)
    x = np.conj(x)
    x = my_DFT(x, n)
    x = np.conj(x)/N
    return x


fftOneA = my_DFT(oneA(256), 256)
fftOneB = my_DFT(oneB(256), 256)

fftTwoA = np.fft.fft(TwoOne(110))*np.fft.fft(TwoX2a(110))
fftTwoB = np.fft.fft(TwoOne(100))*np.fft.fft(TwoX2b(100))

plt.figure(1)
plt.subplot(2, 1, 1)
plt.stem(IFFT(fftOneA, 512))
plt.subplot(2, 1, 2)
plt.stem(IFFT(fftOneB, 512))

plt.figure(2)
plt.subplot(2, 1, 1)
plt.stem(IFFT(fftOneA, 1024))
plt.subplot(2, 1, 2)
plt.stem(IFFT(fftOneB, 1024))


plt.figure(3)
plt.subplot(2, 1, 1)
plt.stem(my_IDFT(fftTwoA, len(fftTwoA)))
plt.subplot(2, 1, 2)
plt.stem(my_IDFT(fftTwoB, len(fftTwoB)))

plt.show()
